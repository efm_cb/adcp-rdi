function sname=adcp_avg(adcp,datafield,Navg,timlim)
% Averages data stored in the adcp structure array as requested in the cell array datafield.
%   The data are placed in a structure variables (e.g. adcp1min)
%	 and saved into a mat file called "avgvel.mat" (provided it doesnt already exixts, it will append a number otherwise)
% Required inputs:
%	adcp: structure of data as returned by ADCP code, with a mtime variable and preferably the z (height) variable for bottom -mounted/moored adcp
%	datafield: cell array of variables to process (e.g. 'enu','beam','hpr', 'corr','pressure',etc)
%   Navg:vector desired avg interval in seconds 
%   Fs: sampling interval in Hz
% Outputs: 
%	sname: name of mat file where data saved 
% Optional:
%   timlim=[stim etim] the start and end time of data to average if you
%   	want to remove the out of water in the datenum format
%   	stim=datenum(2012,4,4,12,0,0);
%  		 etim=datenum(2012,4,22,6,55,0);
%   Navg=[60 120 300 600]; % 1, 2, 5 and 10 min
% Created by CBluteau in May 2015, because I'm sick of doing it after the fact in scripts
%% Improvements & Limitations
%		Not all datafields will be supported, it's a work in progress
% 		Could output a structure with all the structures, but given the shear volume of data, I prefer saving as we go so that I can restart a given Navg
%	Should remove fields not used in adcp, to make it smaller/faster fpr the code to handle
sname='avgvel';


cc=1; % creating new file name if there's already saved avg data
while exist(strcat(sname,'.mat'),'file')~=0
	sname=strcat(sname,'_',num2str(cc));
	cc=cc+1;
end


if nargin<4 
	stim=adcp.mtime(1);
	etim=adcp.mtime(end);
else
	stim=timlim(1);etim=timlim(2);
end

if ~isfield(adcp,'z')
	warning('adcp.z variable wasnt set, using the instrument ranges for z/depth variable instead')
	adcp.z=adcp.config.ranges;
	adcp.zinfo='z is the range from the instrument as it wasnt specified/created by the user from knowledge of where the instrument was sitting';
end
 

ind=find(adcp.mtime>=stim & adcp.mtime<=etim);
 
dT=round(mean(diff(adcp.mtime(ind))*24*3600)); % mean samping interval in seconds
if any(Navg<dT) 
	warning('Youve specified an avg period Navg smaller than sampling interval, Im gonna skip it');
	ind=find(Navg>dT); % removing those greater
	Navg=Navg(ind);
end

 %% Code now that the inputs have been checked 
% Load desired datafield to avg  
 

for ii=1:length(Navg)
	if Navg(ii)<60
		 varname=strcat('avg',num2str(Navg(ii)),'s');
	else
		varname=strcat('avg',num2str(round(Navg(ii)/60)),'min');
    end
     eval(sprintf([varname,'.z=adcp.z;'])); % adding depth  variable
	
        
   for jj=1:length(datafield)  % Could make this more modular
		switch datafield{jj}
			case{'pressure'}
				[ntim ndat,ci, gdpts]=bin_ts_adcp(adcp.mtime(ind),adcp.pressure(1,ind)',dT,Navg(ii));
	
			case {'enu'}  
                 tf={'east_vel','north_vel','vert_vel','error_vel'};
                if isfield(adcp,'enu')==0
                    for pp=1:4
                        adcp.enu(:,pp,:)=adcp.(tf{pp});
                    end
                end  
                [ndat, ntim,gdpts]=doZbinning(adcp.enu(:,:,ind),adcp.mtime(ind),dT,Navg(ii),length(adcp.z));   
%                  for pp=1:4
%                     adcp.(tf{pp})=adcp.enu(:,pp,:);
%                  end
        
				eval(sprintf([varname,'.enugdpts=gdpts;'])); 
			case {'beam'}
				for kk=1:length(adcp.z) 
					[ntim ndat(:,:,kk), ci gdpts(:,kk)]=bin_ts_adcp(adcp.mtime(ind),[adcp.bi(kk,ind)' adcp.b2(kk,ind)' adcp.b3(kk,ind)' adcp.b4(kk,ind)'],...
						dT,Navg(ii));
				end
			case{'hpr'} % heading,pitch, roll 
				[ntim ndat,ci, gdpts]=bin_ts_adcp(adcp.mtime(ind),[adcp.heading(1,ind)' adcp.pitch(1,ind)' adcp.roll(1,ind)'],dT,Navg(ii));
	
			case{'corr','intens'}
                
                [ndat, ntim, gdpts]=doZbinning(adcp.(datafield{jj})(:,:,ind),adcp.mtime(ind),dT,Navg(ii),length(adcp.z));

		
			otherwise
				error(['Variable: ', datafield{jj},'  isnt supported yet']); % this check should be placed before the switch
		end
 
	 eval(sprintf([varname,'.%s=ndat;'],char(datafield(jj)))); 
	 clear gdpts ndat; 
 	end
     
     eval(sprintf([varname,'.tim=ntim;'])); % adding time variable
    
	clear ntim;
	if ii==1;
		save(sname,varname,'-v7.3');
	else
		save(sname,varname,'-append');
		
	end
    clear avg*;
end
end % end of functuin


function [ndat,ntim, gdpts]=doZbinning(data,tim,dT,Na,nz)
% nz=nbre of z bins
    for kk=1:nz
        [ntim tmpdat, ci tmpgdpts]=bin_ts_adcp(tim,squeeze(data(kk,:,:))',dT,Na);

        tmpgdpts=nanmean(tmpgdpts,2);
        if kk==1
            ndat=NaN([length(ntim) 4 nz]);
            gdpts=zeros([length(ntim) nz]);
        end
        ndat(:,:,kk)=tmpdat;
        gdpts(:,kk)=tmpgdpts;
   end
end
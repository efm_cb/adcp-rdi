function [new_datestmp newdata ci gdpts]= bin_ts_adcp(datestmp,data,aintval,dintval)
% program to create mean time series of time scale specified by user. 
% Box-car with timestamp centered at the middle of the interval.
% It will add confidence intervals to the mean assuming a normal distribution. 
% The confidence interval will be a function of the number of data points are not NaN in the interval.
% Calculates a mean for the interval regardless if there is a lot of missing data (i.e uses nanmean.m or gmean.m rather than mean)
% Meant to be used for establishing the accuracy of mean values from data sets with bad/poor records (e.g after ADV spikes removal)
% 
% Differs from subhourlyts.m as the current script is more for evaluating how much data is missing over the averaging period rather than for calculating stats like in subhourlyts
% Required Input: 
%	datestmp and 'data' represent vectors of the datestmp (matlab string date OR julian days)  and data. EVENLY SPACED
%	'aintval' : is the (available) interval in seconds of the sub-hourly data , For example, if 15 min data aintval=15*60s, if data taken every 30sec then aintval=30, or if every 1s then aintval=1
%			the available interval is specified more for data quality and control. You could just do this script by quering the data.
%	'dintval': is the desired data interval in seconds e.g if you want 15min average from data collected every 1 sec, then aintval=1 and dintval=15*60. dintval must be greater than aintval
%               
% Output: 
%	'new_datestmp'=the serial time stamp (make sure more significant digits are used when writing to file) 
%	'new_data'=represents the calculated mean of the interval, size=[m n]
%   Make this optional
%	'ci'=3D matrix (m,n, 2) two column with the upper and lower confidence interval
%	'gdpts'=returns the proportion of the interval that has finite values e.g if N=5min and half the values are presend gdpts=0.5 for that time interval
% Uses:
%   the toolbox that has nanmean  otherwise you can use gmean which return the mean and sum of finite values (e.g A=[NaN 5 10], gsum(A)=15 where sum(A)=NaN)
%
% Created by C.Bluteau
% Last Modified: March 6, 2009
% Improvements and limitations: 
%		Vectorise program for speed
%________________________________________________________________________

if aintval>dintval
	error('The desired data interval must be greater than the available one in the data set');
end

if isequal(round(dintval./aintval),dintval./aintval)==0
	error('The available data frequency must be a multiple of the desired frequency. You cant get the 25 sec average if the data is collected every 10sec');
else
	N=dintval./aintval;
end

if any(abs(diff(datestmp)*24*3600)>1.1*aintval)
    inder=find(diff(datestmp)*24*3600>1.1*aintval);
    disp('Data gap at rows. Unevenly spaced data. Adding data points with NaN, to make it evenly spaced');disp(length(inder))
    [datestmp,data]=fill_ts_adcp(datestmp,data,aintval);
end

[recordlength m]=size(data);
TRANSP=false;
if m>recordlength
    data=transpose(data);
    [recordlength m]=size(data);
    TRANSP=true;
end
disp(size(data))
%%=============================================

compteur=1;
rowc=1;

newdata=zeros(ceil(recordlength/N),m);
%ci=zeros(ceil(recordlength/N),2*m);
while compteur<=recordlength	
   lendat=compteur+N-1; % or N*rowc      
   new_datestmp(rowc)=datestmp(compteur)+(N*aintval/2)/(3600*24); % the datestmp mid-way through the interval, str date 
   valb(1:N,1:m)=NaN;
   valb(1:N,1:m)=data(compteur:lendat,:); % the data trying to retreive(wind,temp,etc)   	
   gdpts(rowc,:)=sum(isfinite(valb))./N;
   newdata(rowc,:)=nanmean(valb); % the interval mean  
   for jj=1:m
       [ht,p,ci(rowc,jj,1:2)] =ttest(valb(:,jj));
   end
   
   compteur=compteur+N;
   rowc=rowc+1;
   if compteur+N>recordlength 	% dealing with the end of the record, basically doing as above 
		lendat=recordlength-compteur+1;
		new_datestmp(rowc)=datestmp(compteur)+(N*aintval/2)/(3600*24);
   		valb(1:N,1:m)=NaN;
    	valb(1:lendat,:)=data(compteur:end,:); % the data trying to retreive(wind,temp,etc)   
		newdata(rowc,:)=nanmean(valb); % the interval mean
		for jj=1:m
            [ht,p,ci(rowc,jj,1:2)] =ttest(valb(:,jj));
        end
            gdpts(rowc,:)=sum(isfinite(valb))./N;
        break;
   end
end

%% Data transpose if required
[m n]=size(new_datestmp);
if n>m
	new_datestmp=transpose(new_datestmp);
end

if TRANSP
	newdata=transpose(newdata);
end
function [ndatestmp,ndata]=fill_ts_adcp(datestmp,data,aintval,tol)
% Fill in timeseries that have been sampled evenly but that have missing rows of data
% e.g. data=[1 2 30 4 56] and datestmp=[306 307 308 310 311], aintval=1
% will yield:
%   ndatestmp=[306 307 308 309 310 311] and ndata=[1 2 30 NaN 456];
% Required inputs:
%   datestmp: array of date stamp in days (matlab dates are in days)
%   aintval: sampling interval in seconds
%   data: original data, can be a matrix. The analysis will be done along
%       the longest dimensions (ie if more rows than col, each col will "filled
%       in" with NaNs
%Optional:
%   tol: tolerance for what is considered a gap (e.g tol=1.5, means the gap
%       must be larger than 1.5 times the time stamps. By default, tol=1.01
% Improvements and Limitations
%   If tol is too low, it may take a long time to run, possibly it's just
%   the time stmps that are all over the place (e.g SBE sampling at 1Hz, the clock isn't perfect)
%_CBluteau: Greatly revamped in 2015 to be faster, and more checks.
%_Uses:
%   filldate.m 
%________________________________________________________________
%% Data checks
if nargin<4
    tol=1.01;
end
[m, n]=size(datestmp);
if m<n
    datestmp=transpose(datestmp);
end


[m n]=size(data);
TRANSP=false;
if n>m
    data=transpose(data);
    [m n]=size(data);
    TRANSP=true;
end
%% Main script
dT=aintval/(24*3600); % in days
df=diff(datestmp)./dT; % interval in sec/normalized by the sampling interval.. so should be 1. 
% df-1= nbre of samples missing in each gap
%tim=datestmp(1):dT:datestmp(end);
inder=find(df>tol); % location of gaps
lrec= round((datestmp(end)-datestmp(1))./dT)+1;
ndatestmp=filldate_adcp([datestmp(1) datestmp(end)],1./aintval,lrec);

ndata=NaN(lrec,n);
ngaps=length(inder);% nbre of gaps

disp(['Number of gaps found:  ',num2str(ngaps)]);

if ngaps==0
    return;
end
 %% Try better implementation
 % haven't tried is it works with matrix of data
%ndata = interp1gap(datestmp,data,ndatestmp,dT*tol); %'interpval',NaN 
 
% place orig data into cell arrays 
data_orig{1}=data(1:inder(1),:);
tim_orig{1}=datestmp(1:inder(1));
for ii=2:ngaps 
   ind=inder(ii-1)+1:inder(ii);
   data_orig{ii}=data(ind,:);
   tim_orig{ii}=datestmp(ind);
end
data_orig{ii+1}=data(inder(end)+1:end,:);
tim_orig{ii+1}=datestmp(inder(end)+1:end);

% %% Lets try assigning data to the right place in ndatestmp
% 
[ndata,ndatestmp] = join_ts_adcp(data_orig,tim_orig);
% for ii=1:length(tim_orig)    
%     ind=find(ndatestmp<=tim_orig{ii}(end) & ndatestmp>=tim_orig{ii}(1));
%     dl=length(ind)-length(tim_orig{ii});
%   
%     if dl~=0 % dealing with clock tolerances issues (I'm assuming you can only be off by 1 point)
%         if dl<0 % not enough data points
%             tmp=(tim_orig{ii}(1)-ndatestmp(ind(1)));
%             if tmp<0  % need to add 1 at start
%                  
%                 ind=ind(1)-1:1:ind(end);     
%             else % add at the end
%                 ind=ind(1):1:ind(end)+1;
%             end
%         else % too many data points recovered from ind 
%             warning('CEB: havent tested this part of the code')
%             tmp=(tim_orig{ii}(end)-ndatestmp(ind(end)));
%             if tmp<0 % one too many point at the end
%                 ind=ind(1):1:ind(end)-1;
%             else
%                 ind=ind(2):1:ind(end);
%             end
%         end
%     end
%     ndata(ind,:)=data_orig{ii};
% end


%% Transpose
if TRANSP
    ndata=transpose(ndata);
end
    
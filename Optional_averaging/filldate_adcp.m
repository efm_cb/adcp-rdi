function datestmp=filldate(datestmp,Fs,le)
%function datestmp=filldate(datestmp,Fs,le)
% This effectively fill in 1s recorded datestmp (from the MP and Nortek ADV)
% to match the sampling freq 'Fs'  of the other 'rapid' channels
% From my playing around, it actually works irrespective of the sampling
% rate of datestmp...
%Required input:
%       le: final desired length of the filled in datestmp record.
%       Essentially this is the length of the 'rapid' channel you're trying
%       to match
% Improvements & Limitations
%   Calculate what the datestmp interval is (if different than 1s) and fill
%       in accordingly...
%   Supply what the total length of the 'fast' channel to fill match it
%   exaclty
%  It appears thefinal ts is not EXACTLY evenly spaced when fast samping (say Hz range) due to
%  rounding/computer limitations (there are only so many bits Matlab can
%  handle 64Hz, on average the new datestmp will be 64 Hz, but amongst
%  timestaps the Fs could be 64.018 or 63.9$#$...

%ndatestmp=zeros([le 1]);
T=(datestmp(end)-datestmp(1)).*(24*3600); % dur in seconds?
dT=1/Fs; % dT in sec
M= mod(T,dT);
n=floor(T./dT); % min number of samples
oldend=datestmp(end);
nend=datestmp(1)+(le-1)*dT/(24*3600); % Making sure the end timestamp is a multiple Fs of the first one (guarantees a perfectly spaced vector)
datestmp=datestmp(1):dT/(24*3600):nend;

% for cc=2:le
%     datestmp(cc)=datestmp(cc-1)+dT/(24*3600);
% end
%% Appending/shortening if needed

    newl=length(datestmp);
    if newl>=le % too long(or equal)
        datestmp=datestmp(1:1:newl);
    else % too short
        cc=0;
        ml=le-newl; % missing stamps
        while cc<ml % tacking the missing datestmp  at the end
            cc=cc+1;
            datestmp(newl+cc)=datestmp(newl+cc-1)+dT/(24*3600);
            %cc=cc+1; 
        end
    end


diffend=(datestmp(end)-oldend)*24*3600; 
%disp(['Diff in sec bet end of the supplied datestmp array and the last "filled" in ones: ',num2str(diffend),' s']);
datestmp=datestmp';
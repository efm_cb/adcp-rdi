function [ndata,ntim] = join_ts_adcp(data,tim)
% function [ndata,ntim] = join_ts(data,tim) creates a new timeseries ndata (matrix) and ntim  (vector)
%   after merging several timeseries contained in the cell array "data" corresponding with the cell array "Tim"
%   The number of timeseries appended together corresponds to the number of
%   cells in data and tim.
%   Each individual timeseries are sampled evenly (e.g. after using fill_ts)
%       but time gaps exists between the end of timeseries and the beginning of the next one in "data" 
%   This function was created to handle high frequency datasets (e.g. ADV turbulence) recorded by loggers that stop 
%       and restart a new file when a certain filesize is exceeded.
%       The start/stop sequence results in short time gaps and we like to work with 1 timeseries per logger.
%       The gaps are filled with NaNs
%Required inputs:
%   data: cell array, where each cell represents data from a file of a logger 
%           (e.g. enu resulting from multiple .vec ADV files for the same instrument)
%           The order of the data must be chronological (1st cell, 1st
%               datafile collected, last cell=last datafile collected)
%           The data in each cell (Can and should) have
%               variable length of records
%   tim: cell array of timestamps, each cell has the same length  data
% Outputs:
%   ndata: one matrix of the appended data (e.g., enu for the entire
%       deployment if one ADV instrument)
%   ntim: corresponding time variable
% Created by CBLuteau in Apr 2015
% Improvements & Limitations:
%      Meant to be run with high frequency data (faster than 1Hz), not sure
%           how it behaves with slower sampling rates given the Fs calcs
%      DO a check that the original timeseries are evenly spaced and given in
%           chronological order  in the cell arrays.
%% Input checks
md=length(data);
mt=length(tim);
if md~=mt
    error('Both arrays must have asme length');
end

for ii=1:length(md)
	ld=length(data{ii});
	lt=length(tim{ii});
	if lt~=ld
		error(['Both data and tim in cell ',num2str(ii),' must have the same length ot record']);
	end
end

n=min(size(data{1})); % assumed long timseries and a few columns of data
%% Establish size of gap arrays and original arrays
for ii=1:md
    Fs(ii)=round(1./mean(diff(tim{ii})*24*3600));
    Npts(ii)=length(tim{ii});
end

if all(Fs==mean(Fs))
    Fs=mean(Fs);
else
    error('Not all timeseries supplied have same sampling frequency');
end

%Npts=sum(Npts);
for ii=1:md-1
    le(ii)=round((tim{ii+1}(1)-tim{ii}(end))*24*3600*Fs)-1;
    st=tim{ii}(end)+1/(Fs*24*3600);
    et=tim{ii+1}(1)-1/(Fs*24*3600);
   if le(ii)>0
    timgap{ii}=filldate_adcp([st et],Fs,le(ii));
    datgap{ii}=NaN(le(ii),n);
   else
      timgap{ii}=[];
      datgap{ii}=[];
   end
end

%ntmp=datestmp(inder(ii))+aintval/(24*3600):aintval/(24*3600):datestmp(inder(ii)+1)-aintval/(3600*24);
    

%% Initialize new arrays
%totPts=sum(Npts)+sum(le);
% ntim=NaN(totPts,1);
% ndata=NaN(totPts,n);

%% Assign data and tim to matrix ntim and ndata


ntim=tim{1};
ndata=data{1};
for ii=2:md
    if le(ii-1)>0
   ntim=[ntim;timgap{ii-1}]; 
   ndata=[ndata;datgap{ii-1}]; 
    end
   ntim=[ntim; tim{ii}];
 
   ndata=[ndata; data{ii}];
end

%% Clean up  small time/clock accuracy issues. ot necessary really.. but nice
ntim=filldate_adcp([ntim(1) ntim(end)],Fs,length(ntim));
function [adcp, postqaqc]=adcp_postclean(adcp, ADCPheight, declination,postqaqc,updown)
% process .mat file exported from binary to mat AFTER using adcprdi.m
%Required inputs:
%	adcp- adcp structure as obtained from adcprdi
%   output: file name/directory to save the output arguments to a file. 
%           If set to [] then nothing will be saved
%	ADCPheight- height of transducer faces above seabed in metres
%	declination- angle of declination used to correct magnetic compass (in
		%degrees.  
%		set declination to [] if you want to let the software use the one programmed onto the instrument
		%declination is positive when magnetic north is east of true 
		%north and therefore velociities have to undergo a counter-clockwise rotation, 
		%and negative when it is to the west.
	
%Optional input:		
	%postqaqc= Structure of limits for various qa/qc thresholds/variables done post beam to enu conversion (and post any3 beam sln)
	%	.error_vel: max error vel to accept the given depth bins at timestep ii (Default is 1m/s)
	%	.RSSI_bump_det= difference in echo amplitude used to locate the surface (default is  12) when there is no pressure sensor
	%	.tuse_echo= 1/0 force the use of the echo amplitude diff (RSSI_bump_det) to locate surface	which is useful when the pressure sensor fails (and there's some junk data)
%				default=0 no forcing
	%   .maxpitchroll=15 (default as per RDI stsndard specs for WH)
    %   .depth= depth of water to aid in choping off the the surface values
    %       using .RSSI_bump_det. Defauts to the max cell range+adcp height
    %       which might be totally WRONG. An error message will appear
        
% Versions
%Nicole Jones created original version 23/07/2014
% CP Sep 2015. modified the code to acount for up or downward looking adcp in the calculation of z, a input variable updown is added, 
%  updown=1 for uplooking updown=-1 for downlooking    
%CE Bluteau: rewrote/renamed in 2015  from clean_ADCP_data since many of the qa/qc screening that were included here  
%   must be done BEFORE the any3beam sln (corr/fish algo/pct good) in
%   adcprdi.m which is used if only 3 good beams available.

warning('Q for Nicole from CEB, do you want to input declination in degrees or radians, your rot matrix  assumes it was supplied in radians.');
%angleInRadians = degtorad(angleInDegrees); % CEB. uncomment this line once
%it's known how declination is supplied


%% Input checks, set to defaults if necessary
mfields={'error_vel','RSSI_bump_det','tuse_echo','maxpitchroll','depth'};
defmaxdepth=max(adcp.config.ranges)+ADCPheight;
def_val=[1 12 0 15 defmaxdepth]; % defaults
% check postqaqc variables, if some vars missing- assign defaults
if nargin<5 % postqaqc not given
	for jj=1:length(mfields) % removing fields I don't want to fill in, to help out my machine!
    		eval(sprintf(strcat('postqaqc.%s=def_val(jj);'),char(mfields(jj))));
	end
else
	qafields=fieldnames(postqaqc);
	for jj=1:length(mfields) 
    	if ~any(strcmp(mfields{jj},qafields)) % mfields jj wasn't specified by the user
    		warning(['You havent specified postqaqc.',mfields{jj},' so using default value of: ', num2str(def_val(jj))]);
    		eval(sprintf(strcat('postqaqc.%s=def_val(jj);'),char(mfields(jj))));
    	end
	end
end
    
%% Other input checks


% declination=34/60 %E; For tandabiddi-check
if isempty(declination) 
	tdec=0;
else  
    tdec=1; % user has specified declination that wasn't empty. OVERIDE planADCP's one
end

if tdec==1
    if adcp.config.magnetic_var~=0
        disp('A magnetic declination was programmed  into planADCP')
 
        if adcp.config.magnetic_var~=declination
            error('magnetic variation not the same as input by user, using user assigned value. Not quite sure how this should be handled if the data sampled in enu');
        end
    end
else
    declination=adcp.config.magnetic_var;
    disp(['Using the programmed declination ',num2str(declination)]);
end



%End of user checks.

%% Estimating z above seabed of each bin for up- and downward looking ADCP

z(1)=updown*adcp.config.bin1_dist+ADCPheight;
for ii=1:size(adcp.east_vel,1)-1
    z(ii+1)=z(ii)+updown*adcp.config.cell_size;
end


% CB: not sure why std needs to be applied (mean value would do)
if std(adcp.depth)==0  || postqaqc.tuse_echo==1 %if no pressure sensor or user chooses to, then find surface using gradient in echo
    disp('Finding the surface with the echo amplitude');
    %amplitude
    if postqaqc.depth==defmaxdepth
        error('You really need to specify the actual depth of water of your deployment for "chopping" off surface values properly'); % although not sure for mounted ADCP very far from surface, programmed for a portion of the water column
    end
    max_ind=round(0.6*(postqaqc.depth-z(1))/adcp.config.cell_size);
    A=squeeze(mean(adcp.intens,2));
    A_diff_mask=(diff(A,1)<postqaqc.RSSI_bump_det);
    
    col=size(A,1);
    for ii=1:size(A,2)
        A_diff_mask(min(find(A_diff_mask(max_ind:end,ii)==0))+max_ind:col,ii)=0;  %don't let it get caught on large gradients close to bottom therefore put in "3"
    end
    adcp.east_vel(~A_diff_mask)=NaN;
    adcp.north_vel(~A_diff_mask)=NaN;
    adcp.vert_vel(~A_diff_mask)=NaN;
else
    disp(['Finding the surface with the pressure data, sensor located ',num2str(max(adcp.depth)), 'm below surface']);
    %remove data within 10% of surface if depth data is available, i.e., if
    %instrument had a pressure sensor
    depth=adcp.depth + ADCPheight;
    if mean(depth)>0
        zmat=(z'*ones(1,length(depth)));
        pressmat=ones(length(z),1)*depth;
        mask=zmat<pressmat*.9;
        adcp.east_vel(~mask)=NaN;
        adcp.north_vel(~mask)=NaN;
        adcp.vert_vel(~mask)=NaN;
        adcp.totalwater_depth=depth; %CB: total water depth at site using pressure sensor
    end
end


%% Magnetic Declination 
% rotmat is the rotation matrix for magnetic declination correction.
declination=pi*declination/180; % converting to radians for calc (yields same result as degtorad(declination).
rotmat = [cos(declination) -sin(declination); sin(declination) cos(declination)];
% Apply magnetic correction

rotvec = [reshape(adcp.east_vel,size(adcp.east_vel,1)*size(adcp.east_vel,2),1) reshape(adcp.north_vel,size(adcp.east_vel,1)*size(adcp.east_vel,2),1)]*rotmat;  
u = reshape(rotvec(:,1),size(adcp.east_vel,1),size(adcp.east_vel,2)); 
v = reshape(rotvec(:,2),size(adcp.east_vel,1),size(adcp.east_vel,2));

%put into structured array
adcp.east_vel = u;
adcp.north_vel = v;
adcp.z=z;

%% Now mask depth bins with large error vel
% CB. evel_threshold: 2000 (mm/s so 2m/s) (WE command, but if set to
            %0 nothing is done by the ADCP). This could be done after the
            %fact given it has no bearing on the 3beam solution as you'd throw
            % out the entire depth cell
              
mask=adcp.error_vel>postqaqc.error_vel; 
adcp.east_vel(mask)=NaN;
adcp.north_vel(mask)=NaN;
adcp.vert_vel(mask)=NaN;

%% now mask out all depth bins associated with excessive  pitch/roll
mask=find(abs(adcp.pitch)>postqaqc.maxpitchroll | abs(adcp.roll)>postqaqc.maxpitchroll);
adcp.east_vel(:,mask)=NaN;
adcp.north_vel(:,mask)=NaN;
adcp.vert_vel(:,mask)=NaN;

adcp.postqaqc=postqaqc;
%% CB removed this, as I'd rather save  just adcp and any thresholds used to screen the data as a separate step
% if ~isempty(output)
%     save(output, 'adcp','postqaqc','-v7.3')
% end

function bdind=ADCPfishDetection(ampli,ampliDiff)
%function bdind=ADCPfishDetection(ampli,ampliDiff)
%Required inputs:
%   ampli: is the adcp echo intensity (adcp.intens) 3d matrix as obtained
%   from the adcprdi reading function
%   amplidiff: is the threshold used to reject a beam (and/or the entire
%       depth cell) as per the details in the ADCP-RDI manual.
%       rdi recommends ampliDIff of 50, or even as low as 40 (the higher,
%       the more data you're keeping as "good").
% Determine the bad indices for the fish algorithm, 
%according the description in section 7 of adcp coordintate transformation manual form RDI 
% Created by CBluteau in July 2015 to deal with the interference issues of
% one of the  Tshelf moorings 

%if cfg.fls_target_threshold=255 means no fish detection algorithm was done



[m, n, t]=size(ampli); % m depth cells, n (4) beams, t timesteps
bdind=ampli; bdind(:)=0; % 0 gd data, 1=bd data
for ii=1:t % for each timestamp
    tmpampli=squeeze(ampli(:,:,ii));
    [B, Ix]=sort(tmpampli,2);
    tmpbd=Ix; tmpbd(:)=0;
    df=B(:,n)-B(:,1); % highest- lowest
    ind=find(df>ampliDiff); % problematic depth cell
    for kk=1:length(ind) % step B of Fig 6 fish algorithm
        bdbeam=Ix(ind(kk),1); % prob beam
        tmpbd(ind(kk),bdbeam)=1;
    end % potentially these beams can have 3 beam sld provided Step C doesn't apply
    
    
    df=B(:,n)-B(:,2); % high-2nd lowest
    ind=find(df>ampliDiff); % entire depth cell is bad, and cell above it
    
    tmpbd(ind,:)=1; % entire depth cell bad
   
    if ~isempty(ind) % dealing with the cell above it (avoiding out of index removal at top end)
        if ind(end)==m
            if length(ind)>1
                ind=ind(1:end-1); 
            else
                ind=[];
            end
        end
    end
    tmpbd(ind+1,:)=1; % removing entire depth cell above it
    bdind(:,:,ii)=tmpbd;
end

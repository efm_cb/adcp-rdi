function [adcp, cfg,qaqc_thres,maskbd]=adcprdi(fname,numav, NFIRST, NEND, azi,qaqc_thres,up)
% %CP % syntax [adcp, cfg,qaqc_thres,maskbd]=adcprdi(fname,numav, NFIRST, NEND,
% azi,qaqc_thres,up) vel=
% Function to read in velocities measured from moored adcp (beam or enu)
% 1) It uses Rich's pawlowich routine to read the data but changes the names of velocity
% variable to beam if recorded in beam.

% 3) It checks whether up/down i.e. if the binary file is the same as what the user thinks the adcp was pointing!
% 4) It converts beam to enu (geographical) velocities after completing :
%		(a) RDI's bin mapping
%		(b) any three beam solution when 1 beam is "shot" i.e. -32.768 (Rich's script) or corr< user specified value (within this code)
% 5) Corrects/rotates the enu velocity according to the magnetic declination at the site. I have some voodoo checks at the top to see if the binary matches the user specified declination, if not the user specified value is used!
%
%
% Required inputs:
%   fname='filename' of adcp binary file including extension .000
% CP %    up: 1/-1 if adcp moored up/down. What's saved in the file, is the direction is what in when programmed ? It's definitely not the direction it was moored in from me playing with some of the previous data collected
%   numav= num of ensembles to average, typically I set to 1. Do averaging
%   later
%   NFIRST: 1st ensemble in water. Set to [] if you want to read all
%   NEND: last ensemble in water.
%       Setting either NFIRST/NEND=[] will read ALL ensembles. You cant
%       specify just one.
%Optional inputs (Set to [] to ignore)
%   azi:  required for beam to enu tranformaiton. Set to [] to use default azimuth==[270 90 0 180], do ps3 command to get correct one
%	qaqc_thres: structure various cutoffs used in the QA/QC process.
%		qaqc_thres.intens_diff for the fish algortimn will assume an echo intensity diff of >50 b/w beams is bad
%		qaqc_thres.corr is the min correlation of 100 (default) that must be met to retain a bin/beam
%       qaqc_thres.pctgd= 50% of data must be good in a given beam/bin
%       qaqc_thres.ignorebeam=0 (use all 4 beams), and is used to ignore one beam
%           (1 through to 4) if you know it's just consistently bad. i.e. you
%           want to mask 1 beam (e.g. ".ignorebeam=2") and force  a 3 beam solution with the other 3
%           beams (e.g., [1,3,4]).
%   Set qaqc_thres=[] to ignore
% Outputs:
%   adcp: structure of adcp data. Note that an the errorvel is set to EXACTLY 0 when the 3 beam solution was used.
%   cfg: structure of configuration info recorded in the adcp binary
%   qaqc_thres: as defaults will be assigned to fields that weren't
%   assigned by the user
%Optional outputs:
%	maskbd: structure of the masking matrix (3d) where each field represents a QAQC parameter as "requested" in qaqc_thres. Useful for establishing after the fact what screening tossed out your data!
%		The var will be cleared if you dont't request it to be output
%Improvements
%   Add other sampling choices in switch command (sampled in enu, xyz)

%Created by CBluteau, making use of Rich's Pawlowich for reading the binary data and greatly inspired by  some USGS scripts
% USES:
%   binmap (which uses bm2dirV2)
%   bm2xyzeV2 (which uses bm2dirV2)
%   any3beam
%   xyz2enu
%   rdradcp by Rich Pawlowich both his 2006 and 2010 version work

%NLJ modified 23/07/2014 so that  rotation for correction magnetic declination is handled in a separate function. Also,
%allows user to specifiy which ensembles to process and allows input of
%azimouth
%CB added in july 2015 other QAQC checks (corr/fish_algorithmn/pct gd) done before the any3beam as these checks are applied on a given bin/beam.
%	Hence, I removed some of the correlation qa/qc from NLJ postcleanup function and placed it back here.
%% Could become user defined inputs

elev=[-70 -70 -70 -70]; % default elev for 20o


if nargin < 3
    [adcp,cfg]=rdradcp(fname,up);
else
    if or(isempty(NFIRST),isempty(NEND))
        [adcp,cfg]=rdradcp(fname,numav); % -32.768 have been removed here
        
    else
        [adcp,cfg]=rdradcp(fname,numav, [NFIRST NEND]); % -32.768 have been removed here
    end
end


mfields={'intens_diff','corr','pctgd','ignorebeam'};
def_val=[50 100 50 0]; % defaults values corresponding to mfileds
if nargin<6 || isempty(qaqc_thres) % use default qaqc screening parameters
    for jj=1:length(mfields) % removing fields I don't want to fill in, to help out my machine!
        qaqc_thres.(mfields{jj})=def_val(jj);
        % eval(sprintf(strcat('qaqc_thres.%s=def_val(jj);'),char(mfields(jj))));
    end
else
    qafields=fieldnames(qaqc_thres);
    for jj=1:length(mfields)
        if ~any(strcmp(mfields{jj},qafields)) % mfields jj wasn't specified by the user
            warning(['You havent specified qaqc_thres.',mfields{jj},' so using default value of: ', num2str(def_val(jj))]);
            qaqc_thres.(mfields{jj})=def_val(jj);
            %eval(sprintf(strcat('qaqc_thres.%s=def_val(jj);'),char(mfields(jj))));
        end
    end
    
end



%check if century was actually corrected in rdradcp.m and correct if
%cent was not included
if adcp.mtime(1)<datenum(1950,1,0)
    disp('time is being corrected')
    adcp.mtime=adcp.mtime+datenum(2000,0,0);
end

%Include a save command here for large datasets?


if strcmp(cfg.beam_pattern,'convex')==0
    error('Code only supports convex beams... although it would be easy to modify the fct to accomodate this beam pattern (see p.11 of ADCP transformation manual)');
end
%% Assigning stuff for easier use with USGS routines
if up==1
    txtup='up';
else
    txtup='down';
end
[m n]=size(adcp.east_vel); % m depth cells, n timestamps


if ~isempty(azi)
    if length(azi)~=4
        error('Supply a vector of 4 values for azi [270 90 0 180] are defaults if you haven"t done the ps3 command on your ADCP');
    end
else
    disp('Using default azimuth 270 90 0 180');
    azi=[270 90 0 180];
end


[allmask,maskbd]=getmask(adcp,qaqc_thres,cfg); % identify bad data
%Some initialization
ENU=zeros([n  4 m]); ENU(:)=NaN;


%% Assign velocities to correct name
switch cfg.coord_sys
    case{'beam'}
        disp('beam')
        adcp.b1=adcp.east_vel;
        adcp.b2=adcp.north_vel;
        adcp.b3=adcp.vert_vel;
        adcp.b4=adcp.error_vel;
        
end


%% For each timestamps lets do calcs
for ii=1:n % each timestamp (process 1 vertical profile at a time)
    beamdat=[adcp.east_vel(:,ii) adcp.north_vel(:,ii) adcp.vert_vel(:,ii) adcp.error_vel(:,ii)]; %[adcp.b1(:,ii) adcp.b2(:,ii) adcp.b3(:,ii) adcp.b4(:,ii)];
    tmpmask=squeeze(allmask(:,:,ii));
    bdind=find(tmpmask(:)==1);
    beamdat(bdind)=NaN;
    
    
    %binmapping occur  before removal of poor corr data as you want to correct the right bins/beams
    if strcmp(cfg.bin_mapping,'no') % Do bin mapping since not done internally (heading does not change the result-not used)
        beamdat=binmap(beamdat,elev,azi,adcp.heading(ii),adcp.pitch(ii), adcp.roll(ii),txtup);
    end
    
    
    
    switch cfg.coord_sys
        case{'beam'}
            T = bm2xyze_v2(elev, azi, txtup);% Get beam to xyze transformation matrix! Should be the same as ps3 command if deployed down
            
            %%%Any three beam
            %if strcmp(cfg.use_3beam,'no') %thre beam sol not done internally on the ADCP
            % CB removed If statement as any3beam should not be done internally on the ADCP when sampled in beam coordinates
            if qaqc_thres.ignorebeam~=0
                beamdat(:,qaqc_thres.ignorebeam)=NaN;
            end
            [beamdat,three_beam]=any3beam(T,beamdat); % CB. Three beam solutions. The bad beam is recast as a function of the 3 others so that we can use rotate into enu
            %else
            %    three_beam=0;
            %end
            %%%END of ANY three beam
            
            
            tp=length(cfg.sensors_avail)-3; % pitch sensor 1/0 as per the EZ command...)
            if  str2num(cfg.sensors_avail(tp))%==1
                thePitch=atand(tand(adcp.pitch(ii))*cosd(adcp.roll(ii))); % CBluteau as per RDI coordinate transformation manual Eq 19 when EZxxx1xxx is set
                % This transformation needed to optain the correct hpr
            else
                thePitch=adcp.pitch(ii);
            end
            
            M=xyz2enu(adcp.heading(ii),thePitch,adcp.roll(ii)); %Obtain the xyz to enu rotation matrix.
            %T=T; % adjoining the beam2xyz and the xyz2geo rotation matrices
            
            xyzdat=beamdat*T.';
            ENU(ii,:,:)=transpose(xyzdat * M.'); % xyz to enu.
            
            
            %Pinning a zero value to the error vel is a 3 beam solution was
            %used for a given depth cell. It is already close to 0, why
            %even bother? It's nice to track which depth cells have used the any3beam sln via find (val==0);
            if any(three_beam), ENU(ii, 4,three_beam) = 0; end
            
            
            
        case {'earth'}
            ENU(ii,:,:)=beamdat';
            if strcmp(cfg.use_pitchroll,'no')==1
                error('Earth coordinates should use pitch and roll')
            end
            
            
        otherwise
            
            % xyz option
            %             if strcmp(cfg.use_3beam,'no') %thre beam sol not done internally on the ADCP
            %                 [beamdat,three_beam]=any3beam(T,beamdat); % CB. Three beam solutions. The bad beam is recast as a function of the 3 others so that we can use rotate into enu
            %             else
            %                 three_beam=0;
            %             end
            %             % END of ANY three beam
            error('If recorded xyz, the code needs to be adapted to include some QAQC and rotate into enu');
    end
    
end

% Assign back
adcp.east_vel=squeeze(ENU(:,1,:))';
adcp.north_vel=squeeze(ENU(:,2,:))';
adcp.vert_vel=squeeze(ENU(:,3,:))';
adcp.error_vel=squeeze(ENU(:,4,:))';

if nargout <4
    clear maskbd;
end

end

function bdind=removeLowQAQCparam(param,param_thres)
%Remove bins/beams with qa/qc param that don't meat a given param_thresh
% e.g., correlation, echo amplitude (intens) and perc good (corr_thres)
%Outputs: bdind of 1/0 of the same size as param to mask bad data (1=bad)
bdind=param;
bdind(:)=0; % 0 gd data, 1=bd data

ind=find(param(:)<param_thres);
bdind(ind)=1;
end


%% Some MASKING matrixes of 1/0 for bad/good as per section 5.2 of ADCP coordinate manual
function [allmask,maskbd]=getmask(adcp,qaqc_thres,cfg)


maskbd.intens_diff=adcpfishdetection(adcp.intens,qaqc_thres.intens_diff);
maskbd.corr=removeLowQAQCparam(adcp.corr,qaqc_thres.corr);

% Done on the beam velocity only as far as the manual states
if strcmp(cfg.coord_sys,'beam')
    maskbd.pctgd=removeLowQAQCparam(adcp.perc_good,qaqc_thres.pctgd);
else
    maskbd.pctgd=maskbd.corr;
    maskbd.pctgd(:)=0;
end
% Add more screening like low echo intensity using removeLowCorr??

%% Now merge bad indices of various qa/qc criteria into 1 matrix allmask
allmask=adcp.corr; allmask(:)=0; % 0 is good, 1 bad

qafields=fieldnames(maskbd);
for jj=1:length(qafields) % removing fields I don't want to fill in, to help out my machine!
    eval(sprintf(strcat('tmpind=maskbd.%s;'),char(qafields(jj))));
    if size(tmpind)==size(allmask)
        allmask=allmask+ tmpind;
    else % fix the size, namely pct_gd is the culprit as it's one timesetp short for the BUBS, not sure if that's typical
        [mtmp, ntmp, ttmp]=size(tmpind);
        allmask(1:mtmp,1:ntmp,1:ttmp)= allmask(1:mtmp,1:ntmp,1:ttmp)+tmpind;
    end
end
tmpind=find(allmask>0.01); % greater than 0 really, but cant be bothered
allmask(tmpind)=1;


end




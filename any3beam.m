function [beamDat,three_beam]=any3beam(T,theBeamData)
% [theBeamData,three_beam]=any3beam(T,theBeamData)
% Doing the three beam solution when only one beam is bad...
%Required Inputs:
%   T=transformation matrix from beam to instrument axis as obtained from bm2xyzeV2
%   theBeamData: mx4 matrix with m depth cells, and 4 beams (one profile)
%Outputs:
%   beamDat: corrected beam data (the bad beam is reassigned as a function
%           of the other three beams).
%   three_beam: the indices of the depth cells where three beams (exactly)
%           were 'good'.
% Patch any three-beam data by forcing error = 0.
% Created by CBluteau
%   Tested against any3beamLDEO.m, which I adapted from the LADCP/LDEO
%   toolbox http://www.ldeo.columbia.edu/cgi-bin/ladcp-cgi-bin/hgwebdir.cgi/LDEO_IX/)
%   My version is vectorized to deal with all depth bins w/ 3 beams at
%   once, while theirs has a for loop, and thus much slower

three_beam = find(isfinite(theBeamData)*ones(4, 1) == 3);
beamDat=theBeamData;
%disp(length(three_beam))
if any(three_beam)
	data = theBeamData(three_beam, :); % CB selecting depth cells  with exactly 3 "good" beams 
	mask = isnan(data); % CB finding which of the 4 beam is bad
	data(mask) = 0; %CB forcing "bad beam"  to zero instead of NaN. Now the error vel is 0.
	tran = ones(length(three_beam), 1) * T(4, :); % CB matrix with rows representing the depth cells with 3 beams, each row represents the error vel transformation
	err = (data .* tran) * ones(4, 1);% mult the data with the error vel trans matrix
	data(mask) = -err ./ tran(mask); % CB Assigning the bad beam to its value dependent on other 3 beams
	beamDat(three_beam, :) = data;
end



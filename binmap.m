function theBeamData=binmap(theBeamData,theElevations,theAzimuths,...
                    theHeading,thePitch,theRoll,theOrientation)
%function theBeamData=binmap(theBeamData,theElevations,theAzimuths,...
%                   theHeading,thePitch,theRoll)
% Simply removed anything to do with bin mapping from the bm2geo code so
% that it could be used in other circumstances...
%Improvements & Limitations
%   Need to test if this can accomodate a time series of beamdata profiles,
%   and pitch & roll data. I'm not sure the tranformation matrix can...
% Created/modified by Cbluteau
%% BIN MAPPING
[m, n] = size(theBeamData);

% Depth-bins, scaled for 20 or 30 degree nominal beam-angle,
%  based on the z-components of the beam-direction matrix.
%  See reference page 8.

RCF = 180 ./ pi;% go in degrees

if mean(abs(theElevations)) > 65
	s = sin(70 ./ RCF);
else
	s = sin(60 ./ RCF);
end

theBeamDirections = bm2dir_v2(theElevations, theAzimuths, ...
						theHeading, thePitch, theRoll, ...
						theOrientation); % CBluteau,This is like the original bm2dir but with the 'roll' left as is (no change in signs for boat work). 
                    % The bin mapping below needs to account for the pitch/roll of the ADCP which results in some bins lower than others (it's unaffected by the heading).                      
depth_scale = (theBeamDirections(:, 3) ./ s).';
depth_bins = (1:m).' * abs(depth_scale);

% Interpolate the beam data in nearest-neighbor
%  fashion.  (The ADCP uses this scheme internally
%  when recording in earth-coordinates.)

% Added 'extrap' to the interp1 command so it would run in Matlab 6.0, but kept old interp1 for earlier versions
ver = version;  %Determine version of Matlab
ver = str2double (ver(1));
if ver < 6
   for j = 1:n
       theBeamData(:, j) = interp1(depth_bins(:, j),...
           theBeamData(:, j), (1:m).', 'nearest');
   end
else
    % turn off the warning generated in MATLAB 7.0
    s = warning('off', 'MATLAB:interp1:NaNinY');
    for j = 1:n
        theBeamData(:, j) = interp1(depth_bins(:, j),...
            theBeamData(:, j), (1:m).', 'nearest','extrap');
    end
    warning('on',s.identifier);
end
 
% clear all; close all;

 mainpath='./testdata';
%set the following parameters to e used to clean up all of the IMOS ADCP P100 data
%sets 
RSSI_bump_det=12;
declination=1.3;% calculated through geoscience
up=1; 

fname='moored_workhorse'; % Input filenae
tname=fname; % output file name
%% From CB- user defined
qaqc.corr=100; %64 effectively ignores it CB
qaqc.intens_diff=30; % dropped it given it's applied to 1min averaged data. After looking at the plot
postqaqc.RSSI_bump_det=12; %CB
postqaqc.tuse_echo=0; % use pressure data to chop surface
postqaqc.depth=105; %approx water depth from sounder at deployment 
azi=[270 90 0 180]; % NLJ grabbed the actual ones for each instrument in
ADCPheight=4.0;
  
%moorings={'shelf_break','bore_shallow','bore_deep','WW_deep','WW_shallow','lander'};

cd(mainpath)

      
 
[adcp, cfg,qaqc_thres,maskbd]=adcprdi(strcat(fname,'.000'),up,[],[],[],postqaqc,up);
   
%adcp.mtime=adcp.mtime+datenum(2012,1,1)-datenum(12,1,1);% Not sure what went wrong with the deployment file but the year has 2 digits.

input=['Raw_',tname,'.mat'];
save(input, 'adcp', 'qaqc_thres','cfg','-v7.3')


output=strcat('qaqc_',tname,'.mat');
[adcp postqaqc]=adcp_postclean(adcp,output, ADCPheight, declination,postqaqc,up); % saves postqaqc within the code itself.. would rather that not be the case


%% Optional code to strip out excess bins that are out of water... to make file smaller when sampling at 1Hz
%dZ=1:1:7; % desired z    
%nZ=length(adcp.z);
%adcp=strip_adcpbins(adcp,dZ,nZ);
%save(strcat('StrippedZ_',stn),'adcp','postqaqc','cfg')


%% Optional averaging

Navg=[ 240 300];
stim=datenum(2012,4,9,8,0,0);
etim=datenum(2012,4,9,12,0,0);

mfields={'enu','pressure','corr','intens'};%,'corr','intens'}; % desired fields to average like pitch
sname=adcp_avg(adcp,mfields,Navg,[stim etim]); 
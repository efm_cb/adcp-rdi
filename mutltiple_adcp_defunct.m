% DEMO when processing multiple ADCPs
 mainpath='\\uniwa.uwa.edu.au\userhome\Staff1\00068741\Cynthia_ECM94Home\Scratch\';
%set the following parameters to e used to clean up all of the IMOS ADCP P100 data
%sets 
RSSI_bump_det=12;
declination=14.703;% calculated through geoscience

%% From CB- user defined
qaqc.corr=64; %64 effectively ignores it CB
qaqc.intens_diff=50;
qaqc.pctgd=0;
%qaqc.ignorebeam=2;
postqaqc.RSSI_bump_det=12; %CB
azi=[270 90 0 180]; % NLJ grabbed the actual ones for each instrument in
%the field
sname=strcat('Clean_corr',num2str(qaqc.corr,'%3.0f'),'_fish',num2str(qaqc.intens_diff,'%3.0f'),'_igbeam',num2str(qaqc.ignorebeam,'%3.0f'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TSHELF moorings
moorings={'shelf_break','bore_shallow','bore_deep','WW_deep','WW_shallow','lander'};
 kk=6;
%for kk=1:length(moorings) 
    fprintf('Processing %s...\n',moorings{kk})
    switch moorings{kk}
        case 'shelf_break'
            path='\150kHz-11075\';
            ADCPheight=4.28;
            Depth=180; %approx water depth from sounder at deployment 
            NFIRST= 11569; %in water by this ensemble
            NEND=113658;%last ensemble in water
            filestr='New_CCat_BBSUB_RDI_001.000';
        case 'bore_deep'
            path='300kHz- 20089\';
            ADCPheight=3.07;
            Depth=120; %approx water depth from sounder at deployment 
            NFIRST= 18154;
            NEND=166156;
            filestr='T0089000.000';
        case 'bore_shallow'
            path='300kHz-20092\';
            ADCPheight=3.36;
            Depth=110; %approx water depth from sounder at deployment
            NFIRST= 19888;
            NEND=131172;
            filestr='New_CCat_BBSUB_T0092000.000';
        case 'lander'
            path='1200kHz-13664\';
            ADCPheight=0.202;
            Depth=180; %approx water depth from sounder at deployment 
            NFIRST= 163816;
            NEND=1708834;
           % NEND=[];
            filestr='New_CCat_BBSUB_T3664000.000';
        case 'WW_deep'
            path='300kHz-9433\';
            ADCPheight=0.5;
            Depth=120; %approx water depth from sounder at deployment 
            NFIRST= 18703;
            NEND=165051;
            filestr='T9433000.000';
        case 'WW_shallow'
            path='300kHz-1947\';
            ADCPheight=0.5;
            Depth=80; %approx water depth from sounder at deployment 
            NFIRST= 20303;
            NEND=226833;
            filestr='T1947001.000';
    end
    postqaqc.depth=Depth;
%     files=cellstr(ls('*.000'));%
    mkdir([mainpath,path,'\Processed'])

    %[adcp cfg]=adcprdi([mainpath,path,filestr],1,NFIRST,NEND);
    [adcp, cfg,qaqc_thres,maskbd]=adcprdi(filestr,1,NFIRST,NEND,azi,qaqc); % new CB version
    
    input=['Raw_',filestr(1:end-4),'.mat'];
    save(input, 'adcp', 'cfg','-v7.3')

    %now clean up data
    %output=[mainpath,path,'Processed\Clean_',filestr(1:end-4),'.mat'];
    
    output=strcat(sname,'_',filestr(1:end-4),'.mat');
   [adcp, postqaqc]=ADCPpostclean(adcp,output, ADCPheight, declination,postqaqc);
    %save output here or in the script
    %clean_ADCP_data(input, output, ADCPheight, declination,corr_cutoff, RSSI_bump_det, Depth);
    
    clear adcp cfg
%end
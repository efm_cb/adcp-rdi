# RDI Teledyne ADCP Processing

Matlab toolboxes that reads, processes and QAQC raw binary files (e.g, data.000) collected by workhorses, quartermaster and long ranger ADCPs that are either moored or bottom-mounted.
Created mainly because we record the rawest of measurements in beam coordinates.  `EX 00000` [??? check]. 
The code replicates  as much as possible the internal processing  that would have been performed  by the ADCP had recorded in enu coordinates.




Features
--------

- Read raw binary files using Rich Pawlowich's  ADCP functions, then converts velocities into enu coordinates.
- Bin mapping 
- Fish (large obstacle) detection to revert to a 3-beam solution 
- QAQC velocities according to correlation and echo amplitude
- Water surface detection for upward looking ADCP
- Optional averaging toolboxes to create ensemble averaged data that stores the percent good in a user selecting averaging period.

Installation
------------

Download or clone project in your Matlab search path and run:


    demo_adcp.m 

which tests the routines against an example  dataset contained under `./testdata/`:


Contribute
----------

Please run code against the test data prior to updating (pushing) the code.

- [Issue Tracker](https://bitbucket.org/uwaoceandynamics/adcp-rdi/issues/)
- [Source Code](https://bitbucket.org/uwaoceandynamics/adcp-rdi/src)

Code Dependencies
-------------

![alt text](demo_adcpHelp.png "Standard processing")


Support and Documentation
-------

CE.Bluteau at UWA is currently mainting the code.
Eventually, we'll have a more complete documentation hosted [here](http://cynthiabluteau.bitbucket.org/adcpTools/site/) that describes the qa/qc. 

In the meantime, refer to the m files help and PDF file within the directory.

License
-------

The project is licensed under MIT.
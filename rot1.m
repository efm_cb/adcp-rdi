function [rx, ry] = rot1(x, y, deg)

% ROT1 Planar rotation by an angle in degrees.
%  [RX,RY]=ROT1(X,Y,DEG) rotates point X toward
%   Y by angle DEG (in degrees).
%  ROT1 (no arguments) demonstrates itself.

% Uses: BOX.
 
% Copyright (C) 1992 Dr. Charles R. Denham, ZYDECO.
% All Rights Reserved.

% Version of 6-Jul-92 at 22:12:09.633.

if nargin < 1
   [x, y, z] = box(1, 2, 3);
   [x, y] = rot1(x, y, 10);
   [y, z] = rot1(y, z, 20);
   [z, x] = rot1(z, x, 30);
   subplot
   subplot(221)
   plot(x, y, '-'), title('Top View'), xlabel('x'), ylabel('y')
   subplot(223)
   plot(x, z, '-'), title('Front View'), xlabel('x'), ylabel('z')
   plot(y, z, '-'), title('Right View'), xlabel('y'), ylabel('z')
   subplot(222)
   title('This is a BOX.')
   subplot
   return
end

if nargin > 2
   xy = [x(:) y(:)].';
  else
   xy = x;
   deg = y;
end

rcf = 180 ./ pi;
rad = deg ./ rcf;
c = cos(rad); s = sin(rad);

r = [c -s; s c];

z = r * xy;

if nargout < 2
   rx = z;
  else
   rx = z(1, :); ry = z(2, :);
end

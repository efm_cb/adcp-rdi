function adcp=strip_adcpbins(adcp,dZind,nZ)



    vf=fieldnames(adcp);
    for ii=1:length(vf)
        tmp=adcp.(vf{ii});
        if ~isstruct(tmp)
           nDim=size(tmp); 
           zDim=find(nDim==nZ);
           if ~isempty(zDim)
               if length(nDim)>2
                   switch zDim
                       case{1}
                       adcp.(vf{ii})=adcp.(vf{ii})(dZind,:,:);
                       case{2}
                       adcp.(vf{ii})=adcp.(vf{ii})(:,dZind,:);
                       case{3}
                       adcp.(vf{ii})=adcp.(vf{ii})(:,:,dZind);    
                   end
               else
                   switch zDim
                       case{1}
                       adcp.(vf{ii})=adcp.(vf{ii})(dZind,:);
                       case{2}
                       adcp.(vf{ii})=adcp.(vf{ii})(:,dZind);
                   end
               end
           end
        end
    end
end
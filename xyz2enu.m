function [M,H,P,R]=xyz2enu(h,p,r)
%Rotation matrix to go from ADCP xyz to enu coordinates 
% ENU=BEAM*M*Tmat
% M is the rotation matrix, while H,P,R are the rotation for each angle
% heading, pitch and roll in degrees as recorded by the ADCP
%Created by CBluteau in Dec 2013.
%Based on Eq 18 of the ADCP coordinate transformaiton manual (p.19)

ch=cosd(h);
sh=sind(h);

cp=cosd(p);
sp=sind(p);

cr=cosd(r);
sr=sind(r);

%%     
H=[ch sh 0;
    -sh ch 0;
    0 0 1]; % around z


R=[cr 0 sr;
    0 1 0;
    -sr 0 cr]; % around y

P=[1 0 0;
    0 cp -sp;
    0 sp cp]; % around x

M=H*P*R; % as per the ADCP coordinate transformation manual
%% Now we need to deal with the fact that there are 4 beams...
%I'm essentially setting all values in last row/col to 0 except for the one on the diagonal.
% This effectively ensures that the error velocity remains as is through the xyz to enu transformation.
newM=eye(4);
newM(1:3,1:3)=M;
M=newM; clear newM;